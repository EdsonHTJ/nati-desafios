class funcionario:
    codigo = ''
    nome = ''
    codigo_cargo = ''
    def __init__(self,codigo,nome,codigo_cargo):
        self.codigo = codigo
        self.nome = nome
        self.codigo_cargo = codigo_cargo

    def getCodigo(self):
        return self.codigo
    
    def getNome(self):
        return self.nome

    def getCodigo_Cargo(self):
        return self.codigo_cargo

    def __repr__(self):
        return (f"Codigo: {str(self.codigo)}, Nome: {self.nome}, Salario: {cargos[self.codigo_cargo]}\n")


cargos = []
funcionarios = []

def cadastrar_cargo(valor):
    cargos.append(valor)

def cadastrar_funcionario(codigo,nome,codigo_cargo):

    if(codigo_cargo>=len(cargos)):
        return 'Erro, Cargo Inesistente'
    for f in funcionarios:
        if f.getCodigo() == codigo:
            return 'Erro codigo Repetido' 
    newF = funcionario(codigo,nome,codigo_cargo)
    funcionarios.append(newF)
    return 'OK'

def mostrarRelatorio():
    return list(funcionarios)

def MostrarTotalSalarioCargo(codigo_cargo):
    F_in_Cargo = 0 #Numero de funcionarios no cargo de codigo inserido
    if codigo_cargo<len(cargos):
        for f in funcionarios:
            if f.getCodigo_Cargo() == codigo_cargo:
                F_in_Cargo += 1

        return F_in_Cargo*cargos[codigo_cargo]

    else:
        return 'Codigo Invalido'


cadastrar_cargo(222.22)
cadastrar_cargo(1000.22)
cadastrar_cargo(1001.22)
cadastrar_cargo(10)


cadastrar_funcionario(0,'Maria',2)
cadastrar_funcionario(1,'Cleito',3)
cadastrar_funcionario(2,'Paulo',1)
cadastrar_funcionario(3,'Luisa',0)
cadastrar_funcionario(5,'C3po',2)
print(MostrarTotalSalarioCargo(2))
print(mostrarRelatorio())