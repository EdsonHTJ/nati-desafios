arr = input('Insira um array de numeros separados por espaço: \n\n')
arr = arr.split(" ")

arr = list(map(int,arr))



positivos= [] #Array de numeros maiores do que 0
negativos = [] #Array de numeros menores do que 0
produto = -1 #produto iniciado em -1 para ser substituido mais na frente
for element in arr:
    if element>0:                           # Caso o elemento seja maior do que 0, eu adiciono ao array de posisivos
        positivos.append(element)           # e ja multiplico ele com o produto
        if produto == -1:
            produto = element
        else:
            produto = produto * element
    elif element <0 :                       #Caso o numero seja negativo, vou adicionalo no array de negativos
        negativos.append(element)           #para poder tratar mais tarde

if len(negativos)%2==1:                     #se o array de negativos tiver um tamanho impar nosso numero sera negativo
    negativos.sort()                        #portanto devemos checar se ele possui tamanho impar e caso possua é retirado
    negativos = negativos[:-1]              #o elemento de valor maior(o mais proximo de 0)
                                            #Assim os numeros de maior modulo vão permanescer no array e como ele tem um tamanho
                                            #par eventualemnte os negativos vão se cancelar visto que (-x * -y = x*y)

for element in negativos:
    produto  = produto*element


print(produto)

print(positivos+negativos)
